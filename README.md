# welcome to smtch.org  :o)

Hi, 
I am Chris Smith a UK based DevOps engineer working with Unix and IT since 1990.
Living a short 4 hrs ferry ride to Dieppe,France & 2 hrs Train to London, so I prefer to WFH ;o)

![alt text](./ChrisLeighSmith-OutofOffice.JPG "A nice evening at the beach")

This git file repo is for storing DevOps scripts and Useful Technical Books & Tips I've used during my work.

I hope you will find some interesting Links, Code, Tools, Tips and files to download, use, and 'make your own' ?

If you would like to contact me please visit [https://www.linkedin.com/in/chrisleighsmith](https://www.linkedin.com/in/chrisleighsmith)
or follow on twitter [@chrisleighsmith](https://twitter.com/chrisleighsmith)

Have fun !

