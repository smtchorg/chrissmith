This is a simple set of terraform scripts to build One AWS vm in Paris Datacenter
using a bitnami WordPress AMI and ssociated Loadbalancer.

1.Install terraform on your laptop

2.Download all files in this repo

3.Copy all file to a new working directory like ~/my_tf_scripts

4.cd to your new directory

5.Type 'terraform init'

6.Type 'terraform apply'

When scripts complete it outputs the Loadbalancer FQDN

Connect to WordPress site via ELB

Open browser and paste the generated ELB FQDN. 
Due to ELB Healthchecks you may need to refresh browser a few times but after 2 Minutes you should see wordpress welcome screen 

Once complete you can also connect to vm over ssh when the script completes 
By using the WordPress public IP which is output to screen

Firstly import the Chrissmith-test.pem file into the AWS
EC2 Service console

Once the Pem file has been imported as a new keypair connect to remote WordPress vm using the public ip address output at end of terraform init

'ssh -i ChrisSmith-test.pem' bitnami@wordpresspublicip
